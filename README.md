
# http-decoder

## What is it?

This is a plugin for Spider to decode TCP payload containing HTTP communications.
It is also a showcase sample for TCP payload decoding plugins, and may be used at will to create your own plugins.

## How to build it?

- `npm run build` will build the bundle with Parcel.js

To access the source map, you may need to change the url to it at the end of the bundle.

## Dev & local test

### Exposing the plugin as HTTPS

1. Install the localCA certificate authority in your own Browser.
* In Chrome, go to `chrome://settings/certificates`
* In `Authorities` tab, import the file `./server/localCA.pem`
2. Launch `nginx` server deploying the plugin files published in `./dist` folder.  
   `docker compose up -d`
3. You may then import the plugin in Spider `Settings` details, `Plugins` tab.  
   `https://localhost/http-decoder.json`

### Reloading

Default setup does not support hot reloading of the bundle.  
The main issues are that:
* Spider needs to access manifest and bundle in a different domain
* Thus, the server needs to accept CORS requests
* Parcel supports hot reloading but CORS headers cannot be changed in Parcel server
  * `parcel ./x-ssl-cert-decoder.js --cert server/localhost.crt --key server/localhost.key`

For now, you need to rebuild the plugin to test any change.

You will also have to reload it manually anyway on Spider:

* Either using the reload icon in the plugin tab
* Or by simply reloading the page ;)

## How to deploy it?

With Spider plugin store!
  
## Usage
- Load the plugin in Spider Plugins store 
- Make sure the Plugin is active in UI / Settings / Plugins tab
- Select the plugin in TCP content tab
- Enjoy

## Features

- Display HTTP request nicely with URL decoded
- Display HTTP headers nicely
- Display payload in ace
- Chunked transfer encoding
- Gzip / Deflate encoding

## License

MIT

## Authors

- thibaut.raballand@gmail.com
