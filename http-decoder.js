import {injectPlugin} from "@floocus/spider-plugin";
import { HTTPParser } from "@browsery/http-parser";
import toBuffer from 'blob-to-buffer';
let React;

async function httpDecoder({
   inputs: {payload}, //Blob
   parameters: {},
   callbacks: {setDecodedPayload, onShowInfo, onShowError, onShowWarning },
   libs: {React: R, Field, Section, Payload}
}){
    React = R;
    if(payload) {
        //get payload as Buffer
        toBuffer(payload, function (err, input) {
            if (err) throw err;

            try{
                const firstBytes = input.slice(0,4).toString('ascii');
                if(firstBytes === 'HTTP'){ //response
                    const res = parseResponse(input);
                    const language = getLanguageOfPayload(res.headers);
                    setDecodedPayload(
                        <>
                            <Section title={'Info'}>
                                <Field header={'Status code'}>{res.statusCode}</Field>
                                <Field header={'Status message'}>{res.statusMessage}</Field>
                                <Field header={'HTTP version'}>{res.versionMajor}.{res.versionMinor}</Field>
                                <Field header={'Upgrade'}>{res.upgrade ? 'true' : 'false'}</Field>
                                <Field header={'Should keep alive'}>{res.shouldKeepAlive ? 'true' : 'false'}</Field>
                            </Section>
                            <Section title={'Headers'}>
                                <Headers Field={Field} headers={res.headers}/>
                            </Section>
                            {res.body.length > 0 &&
                                <Section title={'Body'}>
                                    <Payload
                                        payload={res.body.toString()}
                                        language={language}
                                    />
                                </Section>
                            }
                        </>
                    );
                }
                else{ //request
                    const req = parseRequest(input);
                    const language = getLanguageOfPayload(req.headers);
                    setDecodedPayload(
                        <>
                            <Section title={'Info'}>
                                <Field header={'Method'}>{req.method}</Field>
                                <Field header={'Url'}>{decodeURI(req.url)}</Field>
                                <Field header={'HTTP version'}>{req.versionMajor}.{req.versionMinor}</Field>
                                <Field header={'Upgrade'}>{req.upgrade ? 'true' : 'false'}</Field>
                                <Field header={'Should keep alive'}>{req.shouldKeepAlive ? 'true' : 'false'}</Field>
                            </Section>
                            <Section title={'Headers'}>
                                <Headers Field={Field} headers={req.headers}/>
                            </Section>
                            {req.body.length > 0 &&
                                <Section title={'Body'}>
                                    <Payload
                                        payload={req.body.toString()}
                                        language={language}
                                    />
                                </Section>
                            }
                        </>
                    );
                }
            }
            catch(e){
                onShowWarning(`Error parsing payload: ${e.message}`);
            }
        })
    }
}

function Headers({Field, headers}){
    const res = [];
    for(let i = 0; i< headers.length; i+=2){
        res.push(
            <Field
                key={headers[i]}
                header={headers[i]}
            >
                {headers[i+1]}
            </Field>
        )
    }
    return res;
}

function parseRequest(input) {
    const parser = new HTTPParser(HTTPParser.REQUEST);
    let complete = false;
    let shouldKeepAlive;
    let upgrade;
    let method;
    let url;
    let versionMajor;
    let versionMinor;
    let headers = [];
    let trailers = [];
    let bodyChunks = [];

    parser[HTTPParser.kOnHeadersComplete] = function (req) {
        shouldKeepAlive = req.shouldKeepAlive;
        upgrade = req.upgrade;
        method = HTTPParser.methods[req.method];
        url = req.url;
        versionMajor = req.versionMajor;
        versionMinor = req.versionMinor;
        headers = req.headers;
    };

    parser[HTTPParser.kOnBody] = function (chunk, offset, length) {
        bodyChunks.push(chunk.slice(offset, offset + length));
    };

    // This is actually the event for trailers, go figure.
    parser[HTTPParser.kOnHeaders] = function (t) {
        trailers = t;
    };

    parser[HTTPParser.kOnMessageComplete] = function () {
        complete = true;
    };

    // Since we are sending the entire Buffer at once here all callbacks above happen synchronously.
    // The parser does not do _anything_ asynchronous.
    // However, you can of course call execute() multiple times with multiple chunks, e.g. from a stream.
    // But then you have to refactor the entire logic to be async (e.g. resolve a Promise in kOnMessageComplete and add timeout logic).
    parser.execute(input);
    parser.finish();

    if (!complete) {
        throw new Error('Could not parse request');
    }

    let body = Buffer.concat(bodyChunks);

    return {
        shouldKeepAlive,
        upgrade,
        method,
        url,
        versionMajor,
        versionMinor,
        headers,
        body,
        trailers,
    };
}

function parseResponse(input) {
    const parser = new HTTPParser(HTTPParser.RESPONSE);
    let complete = false;
    let shouldKeepAlive;
    let upgrade;
    let statusCode;
    let statusMessage;
    let versionMajor;
    let versionMinor;
    let headers = [];
    let trailers = [];
    let bodyChunks = [];

    parser[HTTPParser.kOnHeadersComplete] = function (res) {
        shouldKeepAlive = res.shouldKeepAlive;
        upgrade = res.upgrade;
        statusCode = res.statusCode;
        statusMessage = res.statusMessage;
        versionMajor = res.versionMajor;
        versionMinor = res.versionMinor;
        headers = res.headers;
    };

    parser[HTTPParser.kOnBody] = function (chunk, offset, length) {
        bodyChunks.push(chunk.slice(offset, offset + length));
    };

    // This is actually the event for trailers, go figure.
    parser[HTTPParser.kOnHeaders] = function (t) {
        trailers = t;
    };

    parser[HTTPParser.kOnMessageComplete] = function () {
        complete = true;
    };

    // Since we are sending the entire Buffer at once here all callbacks above happen synchronously.
    // The parser does not do _anything_ asynchronous.
    // However, you can of course call execute() multiple times with multiple chunks, e.g. from a stream.
    // But then you have to refactor the entire logic to be async (e.g. resolve a Promise in kOnMessageComplete and add timeout logic).
    parser.execute(input);
    parser.finish();

    if (!complete) {
        throw new Error('Could not parse');
    }

    let body = Buffer.concat(bodyChunks);

    return {
        shouldKeepAlive,
        upgrade,
        statusCode,
        statusMessage,
        versionMajor,
        versionMinor,
        headers,
        body,
        trailers,
    };
}

const knownMimes = new Map([
    ["application/javascript", "js"],
    ["application/x-javascript", "js"],
    ["application/json", "json"],
    ["application/ld+json", "jsonld"],
    ["application/json-patch+json", "json"],
    ["text/json", "json"],
    ["text/css", "css"],
    ["text/html", "html"],
    ["text/javascript", "js"],
    ["text/xml", "xml"],
    ["application/xml", "xml"],
    ["application/atom+xml", "xml"],
    ["application/atom+xhtml", "xml"],
]);


function getLanguageOfPayload(headers){
    let contentType = '';
    for(let i = 0; i < headers.length && !contentType; i+=2){
        if(headers[i].toLowerCase() === 'content-type'){
            contentType = headers[i+1].toLowerCase().split(';')[0];
        }
    }
    return knownMimes.get(contentType) ?? 'text';
}

injectPlugin({
    id: 'http-decoder',
    type: 'tcp-payload-decode-plugin',
    version: '0.1',
    func: httpDecoder,
    errorMessage: 'Could not decode payload!'
});